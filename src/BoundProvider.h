/* Copyright 2011 Tobias Marschall, Email: tm@cwi.nl 
 * 
 * This file is part of string-cover.
 *
 * string-cover is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * string-cover is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with string-cover.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOUNDPROVIDER_H_
#define BOUNDPROVIDER_H_

#include "BranchAndBoundNode.h"
#include "Solution.h"

class BoundProvider {
public:
	/** Computes lower and upper bound for the given node in the branch-and-bound
	 *  tree. The returned pair contains a lower bound and a (possibly suboptimal)
	 *  solution (which provides an upper bound).
	 *  @param lower_bound Destination where lower bound is to be stored.
	 *  @param substring_weight_sums If non-zero must point to an array with at least
	 *         one element for each substring. Each entry is set such that it equals the
	 *         sum of multipliers of selcted intervals annotated with this substring.
	 */
	virtual std::auto_ptr<Solution> compute_bounds(BranchAndBoundNode& node, double global_upper_bound, double* lower_bound, double* substring_weight_sums) const = 0;
	/** Set minimal length of allowed substrings. Set to -1 to desable. */
	virtual void setMinLength(int min_length) = 0;
	/** Set maximal length of allowed substrings. Set to -1 to desable. */
	virtual void setMaxLength(int max_length) = 0;
};

#endif /* BOUNDPROVIDER_H_ */
