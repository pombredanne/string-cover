/* Copyright 2011 Tobias Marschall, Email: tm@cwi.nl 
 * 
 * This file is part of string-cover.
 *
 * string-cover is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * string-cover is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with string-cover.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LAGRANGIANBOUNDPROVIDER_H_
#define LAGRANGIANBOUNDPROVIDER_H_

#include "Multipliers.h"
#include "BranchAndBoundNode.h"
#include "BoundProvider.h"

class LagrangianBoundProvider: public BoundProvider {
private:
	const Multipliers& initial_multipliers;
	/** Step size parameter for Lagrangian optimization. */
	double initial_mu;
	/** Number of non-improving iterations after which the step size is decreased. */
	int allowed_nonimproving_iterations;
	int allowed_nonimproving_iterations_root;
	/** Factor by which mu is decreased. */
	double mu_decrease_factor;
	/** When mu drops below this threshold, we terminate. */
	double mu_threshold;
	double mu_threshold_root;
	int min_length;
	int max_length;
	bool verbose;
public:
	LagrangianBoundProvider(const Multipliers& initial_multipliers);
	virtual ~LagrangianBoundProvider();

	/** Iteratively solves a shortest path problem on the index graphs and adapts the
	 *  multipliers in each iteration. Returns a lower bound for the optimal
	 *  solution and a (possibly suboptimal) solution. The new multipliers are stored
	 *  in the given node.
	 *  Respects included/forbidden strings as given by branch-and-bound node.
	 */
	virtual std::auto_ptr<Solution> compute_bounds(BranchAndBoundNode& node, double global_upper_bound, double* lower_bound, double* substring_weight_sums) const;
	virtual void setParameters(double initial_mu, int allowed_nonimproving_iterations, int allowed_nonimproving_iterations_root, double mu_decrease_factor, double mu_threshold, double mu_threshold_root);
	virtual void setMinLength(int min_length);
	virtual void setMaxLength(int max_length);
	virtual void setVerbose(bool verbose);
};

#endif /* LAGRANGIANBOUNDPROVIDER_H_ */
