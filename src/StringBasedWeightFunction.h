/* Copyright 2011 Tobias Marschall, Email: tm@cwi.nl 
 * 
 * This file is part of string-cover.
 *
 * string-cover is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * string-cover is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with string-cover.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STRINGBASEDWEIGHTFUNCTION_H_
#define STRINGBASEDWEIGHTFUNCTION_H_

#include <fstream>
#include <boost/unordered_map.hpp>

#include "WeightFunction.h"

class StringBasedWeightFunction: public WeightFunction {
	boost::unordered_map<std::string,double> weight_by_string;
	bool is_integer;
	double default_weight;
public:
	/** Construct weight function by reading the weights from a file. */
	StringBasedWeightFunction(std::ifstream& f, double default_weight);
	virtual ~StringBasedWeightFunction();
	virtual double getWeight(std::string s) const;
	virtual bool isInteger() const;
};

#endif /* STRINGBASEDWEIGHTFUNCTION_H_ */
