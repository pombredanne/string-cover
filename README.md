# Background
A string cover of a set of strings is a set of substrings such that concatenations of these substrings produce the original strings. The minimum string cover problem consists of finding a cover with minimum cost under a given cost function for substrings. This project provides a solver based on Lagrangian relaxation and subgradient optimization for this NP-complete problem.

# Requirements
You need the [BOOST library](http://www.boost.org) and [cmake](http://www.cmake.org ) in order to compile the project.

# Getting started
After you have [downloaded](https://bitbucket.org/tobiasmarschall/string-cover/downloads) and extracted the code or, alternatively, cloned the git repository, you are ready to compile it:
```
cmake .
make
```
After successful compilation, the binary `src/stringcover` exists. Running it without parameters gives a list of allowed options. It processes a plain text file with one input string per line.

# References
A paper on the used algorithm has appeared as

S. Canzar, T. Marschall, S. Rahmann, and C. Schwiegelshohn; [Solving the Minimum String Cover Problem](http://siam.omnibooksonline.com/2012ALENEX/data/papers/019.pdf); Proceedings of the Meeting on Algorithm Engineering and Experiments (ALENEX), 2012.

Please cite this paper if you use the software for your research.

# Problem instances
Test instances for the string cover problem are available for [DOWNLOAD](https://bitbucket.org/tobiasmarschall/string-cover/downloads/string-cover-instances.tar.gz).

# Feedback
Please feel free to contact [Tobias Marschall](http://people.mpi-inf.mpg.de/~marschal) with questions, suggestions, and other feedback.
