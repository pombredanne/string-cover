/*
 * IntervalNumberingTest.cpp
 *
 *  Created on: Sep 26, 2011
 *      Author: tobi
 */

#include <boost/test/unit_test.hpp>
#include "../src/IntervalNumbering.h"

#include <vector>
#include <boost/dynamic_bitset.hpp>

using namespace std;


BOOST_AUTO_TEST_CASE( IntervalNumberingTest ) {
	vector<string> v;
	v.push_back("ABC");
	v.push_back("DEFG");
	v.push_back("H");
	IntervalNumbering in(&v);
	BOOST_CHECK_EQUAL(17, in.size());
	boost::dynamic_bitset<> set(in.size());
	for (size_t string_nr=0; string_nr<v.size(); ++string_nr) {
		for (size_t i=0; i<v[string_nr].size(); ++i) {
			for (size_t j=i+1; j<=v[string_nr].size(); ++j) {
				size_t n = in.getIndex(string_nr, i, j);
				BOOST_CHECK(n < in.size());
				BOOST_CHECK(!set[n]);
				set.set(n);
			}
		}
	}
	set.flip();
	BOOST_CHECK(set.none());
}
